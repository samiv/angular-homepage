// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBQhvoqcVIIT71Vrye2eGSAc5Xr0s1X_FU",
    authDomain: "sami-valkonen.firebaseapp.com",
    databaseURL: "https://sami-valkonen.firebaseio.com",
    projectId: "sami-valkonen",
    storageBucket: "sami-valkonen.appspot.com",
    messagingSenderId: "888022282638",
    appId: "1:888022282638:web:f888d3632370ed5a51b4df",
    measurementId: "G-1K3NQE0J35"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
