import { Component, OnInit } from '@angular/core';
import { CareerComponent } from '../career/career.component';

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.scss'],
})
export class DeveloperComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  scrollToBottom(): void {
    window.scrollTo(0, document.body.scrollHeight);
  }
}
