import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Page } from './home/page';
import { PAGES } from './home/mock-pages';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

export type Pages = 'TRAVEL' | 'SPORT' | 'SOFTWARE';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  pages: Page[] = PAGES;

  activePageIdx = 1;

  observablePage: BehaviorSubject<Page>;

  constructor(private router: Router) {
    this.observablePage = new BehaviorSubject<Page>(PAGES[this.activePageIdx]);
    this.router.events.subscribe((val) => {
      // If navigated page and activePageIdx are out of sync, navigate
      if (val instanceof NavigationEnd && '/' + this.pages[this.activePageIdx].path !== val.url) {
        this.setCurrentPageString(val.url);
      }
    });
  }

  setCurrentPage(curPage: Page): void {
    this.observablePage.next(curPage);
    this.navigateTo(curPage.path);
  }

  setCurrentPageString(nextPagePath: string): void {
    const page: Page = this.getPageBasedOnPath(nextPagePath);
    if (page) {
      this.observablePage.next(page);
      this.activePageIdx = this.pages.findIndex((p) => p.path === page.path);
    }
  }

  getPageBasedOnPath(nextPagePath: string): Page {
    if (nextPagePath === '/') {
      // if navigating to root '/', navigate to default page (developer, index 1)
      return this.pages[1];
    }
    return this.pages.filter((p) => '/' + p.path === nextPagePath)[0];
  }

  rotateRight(): void {
    this.activePageIdx = (this.activePageIdx - 1 + this.pages.length) % this.pages.length;
    this.setCurrentPage(this.pages[this.activePageIdx]);
  }

  rotateLeft(): void {
    this.activePageIdx = (this.activePageIdx + 1) % this.pages.length;
    this.setCurrentPage(this.pages[this.activePageIdx]);
  }

  navigateTo(url: string): void {
    this.router.navigateByUrl(url).then((e) => {
      if (e) {
        console.log('Navigation succesful to: ' + url);
      } else {
        console.log('Navigation failed to: ' + url);
      }
    });
  }
}
