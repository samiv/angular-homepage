export class Skill {
  name: string;
  level: string;
  color1: string;
  color2: string;
}
