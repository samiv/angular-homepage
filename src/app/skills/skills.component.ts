import { Component, OnInit } from '@angular/core';
import { Skill } from './skill';
import { SKILLS } from './mock-skills';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  skills: Skill[] = SKILLS;

  constructor() { }

  ngOnInit() {
  }

  getColor(color1: string, color2: string): string {
    return `repeating-linear-gradient(-40deg, ${color1}, ${color1} 10px, ${color2} 10px, ${color2} 20px)`
  }

}
