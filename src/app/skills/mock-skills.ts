import { Skill } from './skill';

export const SKILLS: Skill[] = [
  {
    name: 'Javascript (React, node, express, fastify)',
    level: '90%',
    color1: '#368f1e',
    color2: '#38bd15'
  },
  {
    name: 'HTML5 + CSS3',
    level: '80%',
    color1: '#bf7111',
    color2: '#ad6207'
  },
  {
    name: 'Java (Spring Framework, Swing)',
    level: '70%',
    color1: '#fc4747',
    color2: '#a31414'
  },
  {
    name: 'SQL (PostgreSQL, MSSQL)',
    level: '65%',
    color1: '#0a14a3',
    color2: '#00087d'
  },
  {
    name: 'Angular 2+',
    level: '45%',
    color1: '#a30a70',
    color2: '#8f0160'
  },
];
