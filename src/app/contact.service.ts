import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Contact } from './contact/contact';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private firestore: AngularFirestore) {}

  getContacts() {
    return this.firestore.collection('contacts').snapshotChanges();
  }

  createContact(contact: Contact) {
    console.error(contact);
    return this.firestore.collection('contacts').add(contact);
  }

  // updateContact(contact: Contact) {
  //   delete contact.id;
  //   this.firestore.doc('contacts/' + contact.id).update(contact);
  // }

  // deleteContact(contactId: string) {
  //   this.firestore.doc('contacts/' + contactId).delete();
  // }
}
