import { Profs } from './profs';

export const PROFS: Profs[] = [
  {
    header: "Current Job",
    paras:
      [
        "asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf",
        "asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf asdfsdaf"
      ],
    imgSrc: "/assets/images/work.png"
  },

  {
    header: "Own Projects",
    paras:
      [
        "asdf",
        "asdf",
        "asdf"
      ],
    imgSrc: "/assets/images/projects.png"
  },

  {
    header: "Background",
    paras:
      [
        "asdf",
        "asdf",
        "asdf"
      ],
    imgSrc: "/assets/images/career.png"
  }
]
