export class Page {
  name: string;
  path: string;
  // backgroundUrl: string;
  pagItemClass: string;
  pageIntroHtml: string;
}
