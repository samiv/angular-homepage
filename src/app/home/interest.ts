export class Interest {
  header: string;
  imgVisible: string;
  imgs: string[];
  paras: string[];
  routerLink: string;
}
