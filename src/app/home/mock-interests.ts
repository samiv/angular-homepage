import { Interest } from './interest';

export const INTERESTS: Interest[] = [
  {
    header: "Sport",
    imgVisible: "/assets/images/sport1.png",
    imgs:
      [
        "/assets/images/sport1.png",
        "/assets/images/sport2.png"
      ],
    paras:
      [
        "asdf",
        "asdf",
        "asdf",
        "asdf",
        "asdf",
        "asdf"
      ],
    routerLink: "interests"
  },
  {
    header: "Travel",
    imgVisible: "/assets/images/travel.png",
    imgs:
      [
        "/assets/images/travel.png",
        "/assets/images/travel1.png",
        "/assets/images/travel2.png"
      ],
    paras:
      [
        "test",
        "testi"
      ],
    routerLink: "interests"
  },
  {
    header: "Tv",
    imgVisible: "/assets/images/tv1.png",
    imgs:
      [
        "/assets/images/tv1.png",
        "/assets/images/tv2.png"
      ],
    paras:
      [
        "test",
        "testi"
      ],
    routerLink: "interests"
  }
]
