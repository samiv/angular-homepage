import { Page } from './page';

export const PAGES: Page[] = [
  {
    name: 'Sport Enthusiast',
    path: 'sport',
    pagItemClass: 'pag-item-left',
    pageIntroHtml: 'Sport',
  },
  {
    name: 'SW Developer',
    path: 'developer',
    pagItemClass: 'pag-item-center',
    pageIntroHtml: 'Fullstack developer &#9679; Java Developer &#9679; Project Manager',
  },
  {
    name: 'Explorer',
    path: 'explorer',
    pagItemClass: 'pag-item-right',
    pageIntroHtml: 'Explorer',
  },
];
