import { NavigationService } from './../navigation.service';
import { Component, OnInit } from '@angular/core';
import { Page } from './page';
import { PAGES } from './mock-pages';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('backgroundFade', [
      state('in', style({ opacity: 1 })),
      state('out', style({ opacity: 0 })),
      transition('in <=> out', [animate('400ms')]),
    ]),
    trigger('rot', [
      state(
        'sport',
        style({
          transform: 'translateZ(-35vw) rotateY(-240deg)',
        }),
      ),
      state(
        'explorer',
        style({
          transform: 'translateZ(-35vw) rotateY(-120deg)',
        }),
      ),
      state(
        'developer',
        style({
          transform: 'translateZ(-35vw) rotateY(0)',
        }),
      ),
      transition('developer => explorer, explorer => sport', [animate('0.3s')]),
      transition('developer => sport', [
        animate(
          '0.3s',
          style({
            transform: 'translateZ(-35vw) rotateY(120deg)',
          }),
        ),
      ]),
      transition('sport => explorer', [
        animate(
          '0.3s',
          style({
            transform: 'translateZ(-35vw) rotateY(-120deg)',
          }),
        ),
      ]),
      transition('sport => developer', [
        animate(
          '0.3s',
          style({
            transform: 'translateZ(-35vw) rotateY(-360deg)',
          }),
        ),
      ]),
      transition('explorer => developer', [
        animate(
          '0.3s',
          style({
            transform: 'translateZ(-35vw) rotateY(0deg)',
          }),
        ),
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  pages: Page[] = PAGES;
  pageState: Page;

  constructor(private navigationService: NavigationService) {}

  ngOnInit() {
    this.getCurrentPage();
  }

  getCurrentPage(): void {
    this.navigationService.observablePage.subscribe((curPage) => (this.pageState = curPage));
  }

  rotateRight(): void {
    this.navigationService.rotateRight();
  }

  rotateLeft(): void {
    this.navigationService.rotateLeft();
  }

  // intHoverIn(interest: Interest): void {
  //   this.hoveredInterest = interest;
  //   this.startInterval(interest);
  // }

  // intHoverOut(interest: Interest): void {
  //   this.hoveredInterest = null;
  //   clearInterval(this.interval);
  //   interest.imgVisible = interest.imgs[0];
  // }

  // startInterval(interest: Interest): void {
  //   let counter = 1;
  //   interest.imgVisible = interest.imgs[counter];

  //   this.interval = setInterval(() => {
  //     counter++;
  //     if (counter === interest.imgs.length) {
  //       counter = 0;
  //     }
  //     interest.imgVisible = interest.imgs[counter];
  //   }, 1400);
  // }
}
