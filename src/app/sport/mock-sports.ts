import { Sport } from './sport';

export const SPORTS: Sport[] = [
  {
    name: 'Padel',
    backgroundColor: 'rgba(158, 201, 89, 1)',
    descriptionHtml: '<p>I have been playing padel for 2 years. It is my favourite sport at the moment.</p>',
    imagePath: '../../assets/images/padel.png',
  },
  {
    name: 'Tennis',
    backgroundColor: 'rgba(203, 217, 102, 1)',
    descriptionHtml:
      '<p>Intermediate tennis player. Playing tennis once a week every summer. BONUS: Crazy Roger Federer fan!</p>',
    imagePath: '../../assets/images/tennis.jpg',
  },
  {
    name: 'Gym',
    backgroundColor: 'rgba(128, 128, 128, 1)',
    descriptionHtml: '<p>Regular gym guy. Two to four times a week since 2008</p>',
    imagePath: '../../assets/images/gym.jpg',
  },
  {
    name: 'Basketball',
    backgroundColor: 'rgba(224, 163, 110, 1)',
    descriptionHtml:
      '<p>Basketball has been my main sport since 1998. I have been playing basketball for 20 years. Former teams: HoNsU (Juniors), ' +
      'BC Jyväskylä (Men 1-division), Pyrintö, Pomppu (Men 2-division), Morbid Basket (Men 3-division)</p>',
    imagePath: '../../assets/images/basketball.jpg',
  },
  {
    name: 'Bouldering',
    backgroundColor: 'rgba(112, 114, 212, 1)',
    descriptionHtml:
      '<p>Bouldering is a good all-around exercise to increase body strength. Once a month sport just for fun without any goals.</p>',
    imagePath: '../../assets/images/boulder.png',
  },
  {
    name: 'Soccer',
    backgroundColor: 'rgba(37, 196, 80, 1)',
    descriptionHtml: '<p>I played football 8 years as a junior. Still one of my favourite sports and playing' +
    ' it in <a href="https://tulospalvelu.palloliitto.fi/category/MH2!lanhl21/tables"><i>Puistofutis Tampere</i></a>.</p>'
    + 'Former junior team: PaRi (Palokan Riento)</p>',
    imagePath: '../../assets/images/soccer.png',
  },
  {
    name: 'Beach Volley',
    backgroundColor: 'rgba(212, 185, 112, 1)',
    descriptionHtml: '<p>Playing beach volley casually in the summer. Volleyball is also very funny!</p>',
    imagePath: '../../assets/images/beachvolley.jpg',
  },
  {
    name: 'Running',
    backgroundColor: 'rgba(240, 110, 136, 1)',
    descriptionHtml:
      '<p>Casually running when the mood is perfect. Two times finisher of Tampere Half Marathon.' +
      'Two times finisher of Energy Run Vaasa.</p>',
    imagePath: '../../assets/images/running.png',
  },
];
