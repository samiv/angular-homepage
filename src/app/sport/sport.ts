export class Sport {
    name: string;
    backgroundColor: string; // class should be declared in styles.scss
    descriptionHtml: string;
    imagePath: string;
}
