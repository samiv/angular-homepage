import { SPORTS } from './mock-sports';
import { Component, OnInit } from '@angular/core';
import { Sport } from './sport';

@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss'],
})
export class SportComponent implements OnInit {
  sports: Sport[] = SPORTS;

  selectedSport: Sport = null;

  constructor() {}

  ngOnInit() {}

  selectSport(sport: Sport): void {
    this.selectedSport = sport;
    this.scrollToAnchor();
  }

  unselectSport(): void {
    this.selectedSport = null;
    this.scrollToAnchor();
  }

  getSelectedBackgroundColor(): string {
    if (this.selectedSport === null) {
      return 'white';
    } else {
      return this.selectedSport.backgroundColor;
    }
  }

  getBackgroundColor(sport: Sport): string {
    if (sport.backgroundColor === null) {
      return 'white';
    } else {
      return 'linear-gradient(170deg,' + sport.backgroundColor + '0%, rgba(255,255,255,1) 100%)';
    }
  }

  scrollToAnchor(): void {
    document.querySelector('#sports-anchor').scrollIntoView({ behavior: 'smooth' });
  }
}
