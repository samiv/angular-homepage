import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {
  WORK_PROJECTS = [
    {
      name: 'Java',
      description: 'Java8 Desktop App (2017 - )',
      imgSrc: '../../assets/images/java_image.png',
      tooltipInfo: 'No Additional Info For This Project',
    },
    {
      name: 'React',
      description: 'Electron Desktop App (2019 (9 months))',
      imgSrc: '../../assets/images/react_image.png',
      tooltipInfo: 'No Additional Info For This Project',
    },
    {
      name: 'C++',
      description: 'C++ & Qt desktop app (6 months)',
      imgSrc: '../../assets/images/c++_image.png',
      tooltipInfo: 'No Additional Info For This Project',
    },
    {
      name: 'Matlab',
      description: 'Matlab 5G physical layer simulator (2015-2017)',
      imgSrc: '../../assets/images/matlab_image.svg',
      tooltipInfo: 'No Additional Info For This Project',
    },
  ];

  OWN_PROJECTS = [
    {
      name: 'Irradiant Core - Homepage',
      description: 'HTML, CSS, JavaScript, PHP based webpage for mobile game',
      imgSrc: '../../assets/images/irradiantcore_image.png',
      tooltipInfo: 'Click to navigate to irradiantcore.com',
      link: 'http://irradiantcore.com',
    },
    {
      name: 'Sekvensoft - Homepage',
      description: 'HTML, CSS, JavaScript, PHP based webpage for game company',
      imgSrc: '../../assets/images/sekvensoft_image.png',
      tooltipInfo: 'Click to navigate to sekvensoft.com',
      link: 'http://sekvensoft.com',
    },
    {
      name: 'Sami Valkonen - Homepage',
      description: 'My personal homepage made with Angular6',
      imgSrc: '../../assets/images/angular_image.svg',
      tooltipInfo: 'Click to see projects gitlab repository',
      link: 'http://sami-valkonen.firebaseapp.com',
    },
    {
      name: 'Jyri - Homepage',
      description: 'MERN stack based homepage for entourage of my best friends. Hosted by Firebase hosting.',
      imgSrc: '../../assets/images/mern.jpg',
      tooltipInfo: 'Click to navigate to the page',
      link: 'https://jyri-34a3e.firebaseapp.com/',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
