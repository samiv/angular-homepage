import { ContactsDialogComponent } from './../contacts-dialog/contacts-dialog.component';
import { ContactService } from './../contact.service';
import { NavigationService, Pages } from './../navigation.service';
import { Component, OnInit } from '@angular/core';
import { Contact } from './contact';
import { Page } from '../home/page';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  currentPage: Page;
  emailRegexpString = '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$';
  emailRegexp: RegExp = new RegExp(this.emailRegexpString);

  model: Contact = new Contact();
  infoMessage: string;
  errorMessage: string;

  constructor(
    private navigationService: NavigationService,
    private contactService: ContactService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.getCurrentPage();
  }

  setInfoMessage(message: string): void {
    this.infoMessage = message;
  }

  setErrorMessage(message: string): void {
    this.errorMessage = message;
  }

  onSubmitMail() {
    if (!this.emailRegexp.test(this.model.email)) {
      this.setErrorMessage('Type valid email address');
      this.setInfoMessage(null);
    } else if (this.model.name && this.model.email && this.model.subject && this.model.message) {
      this.contactService
        .createContact({
          name: this.model.name,
          email: this.model.email,
          subject: this.model.subject,
          message: this.model.message,
        })
        .then((res) => {
          this.setInfoMessage('Sending Contact Form was Succesful');
          this.setErrorMessage(null);
          this.resetFields();
        });
    } else {
      this.setErrorMessage('Fill all fields in order to send contact form');
    }
  }

  resetFields(): void {
    this.model = { name: '', email: '', subject: '', message: '' };
  }

  getCurrentPage(): void {
    this.navigationService.observablePage.subscribe((curPage) => (this.currentPage = curPage));
  }

  showContacts(): void {
    const dialogRef = this.dialog.open(ContactsDialogComponent);
    dialogRef.afterClosed().subscribe((data) => console.log('Dialog output:', data));
  }
}
