import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PrivacyPolicyComponent } from '../privacy-policy/privacy-policy.component';
export const COOKIE_PRIVACY_ACCEPTED = `PRIVACY_ACCEPTED`;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  cookiesPopupVisible: boolean;

  constructor(public dialog: MatDialog, private cookieService: CookieService) {}

  ngOnInit(): void {
    this.cookiesPopupVisible = this.cookieService.get(COOKIE_PRIVACY_ACCEPTED) !== 'true';
  }

  openPrivacyPolicyDialog() {
    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(PrivacyPolicyComponent);

    dialogRef.afterClosed().subscribe((data) => console.log('Dialog output:', data));
  }

  acceptPrivacyPolicy(): void {
    this.cookieService.set(COOKIE_PRIVACY_ACCEPTED, 'true', 10);
    this.cookiesPopupVisible = false;
  }
}
