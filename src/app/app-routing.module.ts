import { SportComponent } from './sport/sport.component';
import { ExplorerComponent } from './explorer/explorer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeveloperComponent } from './developer/developer.component';

const routes: Routes = [
  {
    path: '',
    component: DeveloperComponent
  },
  {
    path: 'sport',
    component: SportComponent
  },
  {
    path: 'explorer',
    component: ExplorerComponent
  },
  {
    path: 'developer',
    component: DeveloperComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
