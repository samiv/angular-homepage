import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Contact } from './contact/contact';

@Injectable({
  providedIn: 'root',
})
export class CmspageService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  url = '../assets/send_mail.php';

  constructor(private http: HttpClient) {}

  contactForm(formdata: Contact) {
    return this.http.post<Contact>(this.url, formdata);
  }
}
