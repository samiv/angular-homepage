import { COUNTRIES } from './mock-countries';
import { Component, OnInit } from '@angular/core';
import { Country } from './country';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.scss'],
})
export class ExplorerComponent implements OnInit {
  countries: Country[] = COUNTRIES;
  selectedCountry: Country = this.countries[0];

  constructor() {}

  ngOnInit() {}

  selectCountry(country: Country): void {
    this.selectedCountry = country;
  }

  nextCountry(): void {
    const idx: number = this.getSelectedCountryIndex();
    if (idx >= this.countries.length - 1) {
      this.selectedCountry = this.countries[0];
    } else {
      this.selectedCountry = this.countries[idx + 1];
    }
  }

  previousCountry(): void {
    const idx: number = this.getSelectedCountryIndex();
    if (idx <= 0) {
      this.selectedCountry = this.countries[this.countries.length - 1];
    } else {
      this.selectedCountry = this.countries[idx - 1];
    }
  }

  getSelectedCountryIndex(): number {
    return this.countries.findIndex(e => e === this.selectedCountry);
  }
}
