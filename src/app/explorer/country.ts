export class Country {
    name: string;
    flagImagePath: string;
    cities: City[];
}

export class City {
    name: string;
    photo1path: string;
    photo2path: string;
    colorClass: string; // class should be declared in styles.scss
}
