import { Country } from './country';

export const COUNTRIES: Country[] = [
  {
    name: 'Hungary',
    flagImagePath: '../../assets/images/flags/hungarian-flag.svg',
    cities: [
      {
        name: 'Budapest',
        photo1path: '../../assets/images/explorer/buda1.jpg',
        photo2path: '../../assets/images/explorer/buda2.jpg',
        colorClass: 'green',
      },
    ],
  },
  {
    name: 'Czech Republic',
    flagImagePath: '../../assets/images/flags/czech-flag.svg',
    cities: [
      {
        name: 'Prague',
        photo1path: '../../assets/images/explorer/praha1.jpg',
        photo2path: '../../assets/images/explorer/praha2.jpg',
        colorClass: 'red',
      },
      {
        name: 'Brno',
        photo1path: '../../assets/images/explorer/brno1.jpg',
        photo2path: '../../assets/images/explorer/brno2.jpg',
        colorClass: 'blue',
      },
    ],
  },
  {
    name: 'England',
    flagImagePath: '../../assets/images/flags/british-flag.svg',
    cities: [
      {
        name: 'London',
        photo1path: '../../assets/images/explorer/london1.png',
        photo2path: '../../assets/images/explorer/london2.png',
        colorClass: 'red',
      },
    ],
  },
  {
    name: 'Italy',
    flagImagePath: '../../assets/images/flags/italian-flag.svg',
    cities: [
      {
        name: 'Milan',
        photo1path: '../../assets/images/explorer/milano1.jpg',
        photo2path: '../../assets/images/explorer/milano2.jpg',
        colorClass: 'green',
      },
    ],
  },
  {
    name: 'Thailand',
    flagImagePath: '../../assets/images/flags/thai-flag.svg',
    cities: [
      {
        name: 'Bangkok',
        photo1path: '../../assets/images/explorer/bangkok1.jpg',
        photo2path: '../../assets/images/explorer/explore.png',
        colorClass: 'blue',
      },
      {
        name: 'Koh Samui',
        photo1path: '../../assets/images/explorer/samui1.jpg',
        photo2path: '../../assets/images/explorer/samui2.jpg',
        colorClass: 'red',
      },
      {
        name: 'Koh Tao',
        photo1path: '../../assets/images/explorer/kohtao1.jpg',
        photo2path: '../../assets/images/explorer/kohtao2.jpg',
        colorClass: 'white',
      },
    ],
  },
  {
    name: 'Indonesia',
    flagImagePath: '../../assets/images/flags/indonesian-flag.svg',
    cities: [
      {
        name: 'Gili Air',
        photo1path: '../../assets/images/explorer/gili1.jpg',
        photo2path: '../../assets/images/explorer/gili2.jpg',
        colorClass: 'red',
      },
      {
        name: 'Ubud',
        photo1path: '../../assets/images/explorer/ubud1.jpg',
        photo2path: '../../assets/images/explorer/ubud2.jpg',
        colorClass: 'green',
      },
      {
        name: 'Canggu',
        photo1path: '../../assets/images/explorer/canggu1.jpg',
        photo2path: '../../assets/images/explorer/canggu2.jpg',
        colorClass: 'black',
      },
      {
        name: 'Mount Batur',
        photo1path: '../../assets/images/explorer/batur1.jpg',
        photo2path: '../../assets/images/explorer/batur2.jpg',
        colorClass: 'blue',
      },
    ],
  },
  {
    name: 'German',
    flagImagePath: '../../assets/images/flags/german-flag.svg',
    cities: [
      {
        name: 'Berlin',
        photo1path: '../../assets/images/explorer/berlin1.jpg',
        photo2path: '../../assets/images/explorer/berlin2.jpg',
        colorClass: 'yellow',
      },
      {
        name: 'Cologne',
        photo1path: '../../assets/images/explorer/cologne1.jpg',
        photo2path: '../../assets/images/explorer/cologne2.jpg',
        colorClass: 'yellow',
      },
    ],
  },
  {
    name: 'Ireland',
    flagImagePath: '../../assets/images/flags/irish-flag.svg',
    cities: [
      {
        name: 'Dublin',
        photo1path: '../../assets/images/explorer/dublin1.jpg',
        photo2path: '../../assets/images/explorer/dublin2.jpg',
        colorClass: 'green',
      },
    ],
  },
  {
    name: 'Poland',
    flagImagePath: '../../assets/images/flags/polish-flag.svg',
    cities: [
      {
        name: 'Krakov',
        photo1path: '../../assets/images/explorer/krakova1.jpg',
        photo2path: '../../assets/images/explorer/krakova2.jpg',
        colorClass: 'red',
      },
      {
        name: 'Auswitch-Birkenau',
        photo1path: '../../assets/images/explorer/ausw1.jpg',
        photo2path: '../../assets/images/explorer/ausw2.jpg',
        colorClass: 'black',
      },
    ],
  },
  {
    name: 'Croatia',
    flagImagePath: '../../assets/images/flags/croatian-flag.svg',
    cities: [
      {
        name: 'Zagreb',
        photo1path: '../../assets/images/explorer/zagreb1.jpg',
        photo2path: '../../assets/images/explorer/zagreb2.jpg',
        colorClass: 'red',
      },
      {
        name: 'Zadar',
        photo1path: '../../assets/images/explorer/zadar1.jpg',
        photo2path: '../../assets/images/explorer/zadar2.jpg',
        colorClass: 'blue',
      },
    ],
  },
  {
    name: 'Greece',
    flagImagePath: '../../assets/images/flags/greek-flag.svg',
    cities: [
      {
        name: 'Thassos',
        photo1path: '../../assets/images/explorer/thassos1.png',
        photo2path: '../../assets/images/explorer/thassos2.png',
        colorClass: 'blue',
      },
      {
        name: 'Rhodos',
        photo1path: '../../assets/images/explorer/rhodos1.jpg',
        photo2path: '../../assets/images/explorer/rhodos2.jpg',
        colorClass: 'black',
      },
    ],
  },
  {
    name: 'Turkey',
    flagImagePath: '../../assets/images/flags/turkish-flag.svg',
    cities: [
      {
        name: 'Alanya',
        photo1path: '../../assets/images/explorer/explore.png',
        photo2path: '../../assets/images/explorer/explore.png',
        colorClass: 'red',
      },
    ],
  },
];
