import { Component, OnInit } from '@angular/core';
import { SUMMER_HEADER, SW_HEADER, SW_JOBS, SUMMER_JOBS } from './mock-jobs';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.scss']
})
export class CareerComponent implements OnInit {

  sw_header: string = SW_HEADER;
  summer_header: string = SUMMER_HEADER;

  sw_jobs = SW_JOBS;
  summer_jobs = SUMMER_JOBS;

  constructor() { }

  ngOnInit() {
  }

}
