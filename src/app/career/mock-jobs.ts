import { Job } from './job';

export const SW_HEADER = 'Software Jobs';
export const SUMMER_HEADER = 'Summer Jobs';

export const SW_JOBS: Job[] = [
  {
    time: '12/2020',
    title: 'Pihlajalinna lääkärikeskukset Oy',
    paras:
      [
        'Fullstack developer using React, Redux, mobx, material-ui in frontend and node, express, fastify, MSSQL, PostgreSQL in backend/db',
      ]
  },
  {
    time: '11/2019',
    title: 'Insta DefSec',
    paras:
      [
        'I had two new roles in two different projects: 1. Project Manager, 2. Scrum Master + Java Developer',
      ]
  },
  {
    time: '01/2019',
    title: 'Insta DefSec',
    paras:
      [
        'I started in a new project developing React+Electron App.',
      ]
  },
  {
    time: '05/2017 ',
    title: 'Insta DefSec',
    paras:
      [
        'I started as a Java Developer at Insta.',
      ]
  },
  {
    time: '10/2015',
    title: 'Tampere University of Technology / Nokia',
    paras:
      [
        'I started my researcer career at TUT and Nokia developing 5G link simulator software.',
      ]
  },
  {
    time: '04/2015',
    title: 'M-Files',
    paras:
      [
        'I had my first job in Technology field at M-Files. My status was Test Engineer Intern.',
      ]
  }
]

export const SUMMER_JOBS: Job[] = [
  {
    time: 'Summers 2012-2014',
    title: 'Valio Oy',
    paras:
      [
        'I worked at warehouse where I received returning items and put them back to production.',
      ]
  },
  {
    time: 'Summer 2011',
    title: 'ISS Oy',
    paras:
      [
        'I worked as a cleaner in schools and construction sites.',
      ]
  }
]
